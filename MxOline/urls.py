"""MxOline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.views.static import serve
from MxOline import settings
from apps.operations.views import IndexView
from apps.organizations.views import OrgView
from apps.users.views import LoginView, LogoutView, SendSms, DynamicLoginPostView, RegisterView
import xadmin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('xadmin/', xadmin.site.urls),
    # TemplateView是class
    path('', IndexView.as_view(), name='index'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('captcha/', include('captcha.urls')),
    # 不做csrf验证, 这里用了ajax
    path('send_sms/', csrf_exempt(SendSms.as_view()), name='send_sms'),
    # 发送动态手机登录提交
    path('d_login/', DynamicLoginPostView.as_view(), name='d_login'),
    # 上传资源
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    # 生产要写， 开发环境不要写
    # re_path(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
    # 机构
    re_path('^org/', include(('apps.organizations.urls', 'organizations'), namespace='org')),
    # 课程
    re_path('^course/', include(('apps.courses.urls', 'courses'), namespace='course')),

    # 用户操作
    re_path('^op/', include(('apps.operations.urls', 'operations'), namespace='op')),

    # 个人中心
    re_path('^users/', include(('apps.users.urls', 'users'), namespace='users')),
    # 富文本
    url(r'^ueditor/',include('DjangoUeditor.urls' )),
]
