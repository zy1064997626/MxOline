#!/bin/python3
# -*- coding: utf-8 -*-
from .models import Course, Lesson, CourseResource, Video, CourseTag, BannerCourse
import xadmin
from xadmin.layout import Fieldset, Main, Side, Row
class LessonInline(object):
    model = Lesson
    extra = 0
    # 第一个用style ， xadmin自身bug
    # style = "tab"
class CourseResourceInline(object):
    model = CourseResource
    extra = 1
    style = "tab"

from import_export import resources

class MyResource(resources.ModelResource):
    class Meta:
        model = Course
class CourseAdmin(object):
    import_export_args = {'import_resource_class': MyResource, 'export_resource_class': MyResource}
    list_display = ['name','desc','details','degree','learn_times', 'students', 'is_banner', 'show_image', 'goto']
    search_fields = ['name', 'desc', 'details', 'degree', 'students']
    list_filter = ['name','teacher__name','desc','details','degree','learn_times','students']
    list_editable = ['degree', 'desc']
    readonly_fields = ["students", "add_time"]
    # exclude = ["click_nums", "fav_nums"]
    ordering = ["-click_nums"]
    model_icon = 'fa fa-envelope-square'
    inlines = [LessonInline, CourseResourceInline]
    style_fields = {
        "details":"ueditor"
    }
    def queryset(self):
        qs = super().queryset()
        if not self.request.user.is_superuser:
            # one to one
            qs = qs.filter(teacher=self.request.user.teacher)
        return qs
    def get_form_layout(self):
        # org_obj 编辑时展示， 新建时不展示
        if self.org_obj:
            self.form_layout = (
                    Main(
                        Fieldset("讲师信息",
                                 'teacher', 'course_org',
                                 # css_class='unsort no_title'
                                 ),
                        Fieldset("基本信息",
                                 'name', 'desc',
                                 Row('learn_times', 'degree'),
                                 Row('category'),
                                 'you_need_know', 'teacher_tell', 'details',
                                 ),
                    ),
                    Side(
                        Fieldset("访问信息",
                                 'fav_nums', 'click_nums', 'students', 'add_time'
                                 ),
                    ),
                    Side(
                        Fieldset("选择信息",
                                 'is_banner', 'is_classics'
                                 ),
                    )
            )
        return super(CourseAdmin, self).get_form_layout()

class BannerCourseAdmin(object):
    list_display = ['name','desc','details','degree','learn_times', 'students', 'is_banner']
    search_fields = ['name', 'desc', 'details', 'degree', 'students']
    list_filter = ['name','teacher__name','desc','details','degree','learn_times','students']
    list_editable = ['degree', 'desc']

    def queryset(self):
        qs = super().queryset()
        qs = qs.filter(is_banner=True)
        return qs


class CourseTagAdmin(object):
    list_display = ['course', 'tag', 'degree', 'add_time']
    search_fields = ['course', 'tag', 'degree', ]
    list_filter = ['course__name', 'tag', 'degree', 'add_time']
    list_editable = ['course', 'tag', 'degree', ]




class LessonAdmin(object):
    list_display = ['course', 'name', 'learn_times']
    search_fields = ['course', 'name']
    list_filter = ['course__name', 'name', 'add_time']



class CourseResourceAdmin(object):
    list_display = ['course', 'name', 'add_time']
    search_fields = ['course', 'name']
    list_filter = ['course__name', 'name', 'add_time']


class VideoAdmin(object):
    list_display = ['lesson', 'name', 'add_time']
    search_fields = ['lesson', 'name']
    list_filter = ['lesson__name', 'name', 'add_time']


xadmin.site.register(Course, CourseAdmin)
xadmin.site.register(BannerCourse, BannerCourseAdmin)
xadmin.site.register(Lesson, LessonAdmin)
xadmin.site.register(CourseResource, CourseResourceAdmin)
xadmin.site.register(Video, VideoAdmin)
xadmin.site.register(CourseTag, CourseTagAdmin)
