# Generated by Django 2.2.17 on 2021-02-22 14:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0014_course_tag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='tag',
        ),
    ]
