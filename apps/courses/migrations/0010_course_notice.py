# Generated by Django 2.2.17 on 2021-02-06 16:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0009_video_learn_times'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='notice',
            field=models.CharField(default='', max_length=300, verbose_name='课程公告'),
        ),
    ]
