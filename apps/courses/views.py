from django.db.models import Q

from apps.courses.models import Video
from django.contrib.auth.mixins import LoginRequiredMixin
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from django.views.generic.base import View
from .models import Course, CourseResource
from apps.operations.models import UserFavorite, UserCourse, UserComment
from apps.courses.models import CourseTag


class CourseListView(View):
    def get(self, request, *args, **kwargs):
        '''获取课程列表'''
        all_course = Course.objects.order_by('-add_time')
        hot_course = Course.objects.order_by('-click_nums')
        # 搜索
        keywords = request.GET.get('keywords')
        s_type = ''
        if keywords:
            all_course = all_course.filter(Q(name__icontains=keywords) | Q(desc__icontains=keywords) | Q(details__icontains=keywords))
            s_type = 'course'
        sort = request.GET.get('sort', '')
        if sort == 'students':
            all_course = Course.objects.order_by('-students')
        elif sort == 'hot':
            all_course = Course.objects.order_by('-click_nums')
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        p = Paginator(all_course, per_page=1, request=request)
        all_course = p.page(page)
        return render(request,
                      'course-list.html',
                      {'all_course': all_course,
                       'sort': sort,
                       'hot_course': hot_course[:3],
                       'keywords': keywords, 's_type': s_type})


class CourseDetailView(View):
    def get(self, request, course_id, *args, **kwargs):
        # 查询当前课程
        course = Course.objects.get(id=course_id)
        course.click_nums += 1
        course.save()
        # 课程和机构是否收藏
        has_fav_course = False
        has_fav_org = False
        if request.user.is_authenticated:
            # 查看用户
            '''
                (1, '课程机构'),
                (2, '授课教师'),
                (3, '公开课程')
            '''
            if UserFavorite.objects.filter(
                    user=request.user, fav_type=3, fav_id=course.id):
                has_fav_course = True
            if UserFavorite.objects.filter(
                    user=request.user,
                    fav_type=1,
                    fav_id=course.course_org.id):
                has_fav_org = True
        # 所有有关课程的tag
        tags = course.coursetag_set.all()
        # 获得所有tag
        tag_list = [tag.tag for tag in tags]
        # 只有外键才可以生成这样模式__
        course_tags = CourseTag.objects.filter(
            tag__in=tag_list).exclude(
            course__id=course.id)
        related_courses = set()
        for course_tag in course_tags:
            related_courses.add(course_tag.course)
        related_courses = list(related_courses)
        #
        related_courses.sort(key=lambda x: x.degree, reverse=True)
        return render(request,
                      'course-detail.html',
                      {'course': course,
                       'has_fav_course': has_fav_course,
                       'has_fav_org': has_fav_org,
                       'related_course': related_courses[:2]})


class CourseLessonView(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, course_id, *args, **kwargs):
        # 查询当前课程
        course = Course.objects.get(id=course_id)
        course.click_nums += 1
        course.save()
        # 用户和课程
        existed_user_course = UserCourse.objects.filter(
            user=request.user, course=course)
        if not existed_user_course:
            user_course = UserCourse()
            user_course.user = request.user
            user_course.course = course
            user_course.save()
            # 学习人数
            course.students += 1
            course.save()
        course_resource = CourseResource.objects.filter(course=course)

        # 学习过该课程的所有同学
        user_courses = UserCourse.objects.filter(course=course)
        user_ids = [i.user.id for i in user_courses]

        related_course = UserCourse.objects.filter(
            user_id__in=user_ids).exclude(
            course=course).order_by('-course__click_nums')

        return render(request,
                      'course-video.html',
                      {'course': course,
                       'course_resource': course_resource,
                       'related_course': related_course})


class CourseCommentView(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, course_id, *args, **kwargs):
        # 查询当前课程
        course = Course.objects.get(id=course_id)
        course.click_nums += 1
        course.save()
        # 用户和课程
        existed_user_course = UserCourse.objects.filter(
            user=request.user, course=course)
        if not existed_user_course:
            user_course = UserCourse()
            user_course.user = request.user
            user_course.course = course
            user_course.save()
            # 学习人数
            course.students += 1
            course.save()
        course_resource = CourseResource.objects.filter(course=course)
        # 学习过该课程的所有同学
        user_courses = UserCourse.objects.filter(course=course)
        user_ids = [i.user.id for i in user_courses]

        related_course = UserCourse.objects.filter(
            user_id__in=user_ids).exclude(
            course=course).order_by('-course__click_nums')
        comments = UserComment.objects.filter(course=course)
        return render(request,
                      'course-comment.html',
                      {'course': course,
                       'course_resource': course_resource,
                       'related_course': related_course, 'comments': comments})


class CourseVideoView(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, course_id, video_id, *args, **kwargs):
        # 查询当前课程
        course = Course.objects.get(id=course_id)
        course.click_nums += 1
        course.save()
        # 用户和课程
        existed_user_course = UserCourse.objects.filter(
            user=request.user, course=course)
        if not existed_user_course:
            user_course = UserCourse()
            user_course.user = request.user
            user_course.course = course
            user_course.save()
            # 学习人数
            course.students += 1
            course.save()
        course_resource = CourseResource.objects.filter(course=course)

        # 学习过该课程的所有同学
        user_courses = UserCourse.objects.filter(course=course)
        user_ids = [i.user.id for i in user_courses]

        related_course = UserCourse.objects.filter(
            user_id__in=user_ids).exclude(
            course=course).order_by('-course__click_nums')
        #
        video = Video.objects.get(id=int(video_id))
        return render(request,
                      'course-play.html',
                      {'course': course,
                       'course_resource': course_resource,
                       'related_course': related_course, 'video': video})
