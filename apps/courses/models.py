from django.db import models
# 对象 -  关系
# 课程  章节  视频  课程资源
#  课程  ---》 章节   一对多
#  章节 ----》 视频   一对多
#  课程 ----》 视频资源 一对多


# 字段种类
# 是否必填值 charfield注意设置字段长度

# Create your models here.
from apps.users.models import BaseModel

from apps.organizations.models import Teacher, CourseOrg
from DjangoUeditor.models import UEditorField
COURSE_CHOICES = (
    ('cj', '初级'),
    ('zj', '中级'),
    ('gj', '高级')
)


class Course(BaseModel):
    '''
    name： 名称
    desc：描述
    learn_times:课程市场
    degree：难度
    students:学习人数
    fav_nums:收藏数量
    click_nums：点击次数
    category：种类
    tag： 标签
    youneed_know: 课程须知
    teacher_tell: 老师告诉你
    details:细节
    image:图片
    '''
    # 老师变了
    teacher = models.ForeignKey(
        Teacher,
        verbose_name='讲师',
        on_delete=models.CASCADE)
    # 课程一定是该老师的
    course_org = models.ForeignKey(
        CourseOrg,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name="课程机构")
    name = models.CharField(max_length=50, verbose_name='课程名称')
    desc = models.CharField(max_length=200, verbose_name='课程描述')
    learn_times = models.IntegerField(default=0, verbose_name='课程时长(单位 min)')
    degree = models.CharField(
        max_length=2,
        verbose_name='课程难度',
        choices=COURSE_CHOICES)
    students = models.IntegerField(default=0, verbose_name='学习人数')
    fav_nums = models.IntegerField(default=0, verbose_name='收藏数量')
    click_nums = models.IntegerField(default=0, verbose_name='点击次数')
    category = models.CharField(
        max_length=20,
        verbose_name='课程类别',
        default='后端开发')

    you_need_know = models.CharField(
        max_length=20, default='', verbose_name='课程须知')
    teacher_tell = models.CharField(
        max_length=20, default='', verbose_name='老师告诉你')
    # details = models.TextField(verbose_name='课程详情')
    details = UEditorField(
        verbose_name='课程详情',
        width=600,
        height=300,
        # 图片路径
        imagePath='courses/ueditor/images/',
        # 文件路径
        filePath="courses/ueditor/files/",
        default='')
    notice = models.CharField(verbose_name="课程公告", max_length=300, default="")
    image = models.FileField(upload_to='courses/%Y/%m', verbose_name='封面图')
    is_classics = models.BooleanField(default=False, verbose_name="是否经典")
    is_banner = models.BooleanField(default=False, verbose_name="是否广告位")

    class Meta:
        verbose_name = '课程信息'
        verbose_name_plural = verbose_name

    def show_image(self):
        from django.utils.safestring import mark_safe
        return mark_safe(
            '''<img width="60%" height="30px"src='{}'>'''.format(
                self.image.url))
    show_image.short_description = '图片'

    def goto(self):
        from django.utils.safestring import mark_safe
        return mark_safe("<a href='/course/{}'>跳转</a>".format(self.id))

    goto.short_description = '课程链接'
    def show_details(self):
        from django.utils.safestring import mark_safe
        return mark_safe(self.details)

    show_details.short_description = '描述'

    def __str__(self):
        return self.name


class BannerCourse(Course):
    class Meta:
        verbose_name = '轮播图'
        verbose_name_plural = verbose_name
        # 数据库不会生成表
        proxy = True


COURSETAG_CHOICES = (
    (1, '等级1'),
    (2, '等级2'),
    (3, '等级3')
)


class CourseTag(BaseModel):
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        verbose_name='课程信息')
    tag = models.CharField(max_length=10, default='', verbose_name='课程标签')
    degree = models.IntegerField(
        verbose_name='标签等级',
        choices=COURSETAG_CHOICES, default=1)

    class Meta:
        verbose_name = '课程标签'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.tag


class Lesson(BaseModel):
    # 外键 数据不存在时， 当前数据级联删/设置为空 （CASCADE/SET_NULL null=True, black=True）
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        verbose_name='课程信息')
    name = models.CharField(verbose_name='章节名', max_length=100)

    learn_times = models.IntegerField(verbose_name='学习时长(单位 min)')

    class Meta:
        verbose_name = '章节'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Video(BaseModel):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, verbose_name='章节名')
    learn_times = models.IntegerField(default=0, verbose_name=u"学习时长(分钟数)")
    url = models.CharField(max_length=500, verbose_name='视频链接')

    class Meta:
        verbose_name = '音频'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

# course 是 CourseResource外键


class CourseResource(BaseModel):
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        verbose_name='课程')
    name = models.CharField(max_length=100, verbose_name='课程名称')
    file = models.FileField(
        max_length=200,
        verbose_name='下载地址',
        upload_to='course/resourse/%Y/%m')

    class Meta:
        verbose_name = '课程资源'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
