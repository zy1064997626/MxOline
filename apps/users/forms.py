#!/bin/python3
# -*- coding: utf-8 -*-
import redis
from captcha.fields import CaptchaField
from django import forms
from django.utils import timezone

from .models import UserProfile
from MxOline.settings import REDIS_HOST, REDIS_PORT


class LoginForm(forms.Form):
    #
    username = forms.CharField(min_length=2, required=True)
    password = forms.CharField(min_length=3, required=True)


class DynamicLoginForm(forms.Form):
    mobile = forms.CharField(min_length=11, max_length=11, required=True)
    captcha = CaptchaField()


class DynamicLoginPostForm(forms.Form):
    mobile = forms.CharField(min_length=11, max_length=11, required=True)
    code = forms.CharField(required=True, min_length=4, max_length=4)

    def clean_code(self):
        # 对code进行验证
        mobile = self.data.get("mobile")
        code = self.data.get("code")
        r = redis.Redis(
            host=REDIS_HOST,
            port=REDIS_PORT,
            db=0,
            charset="utf8",
            # redis 字节码
            decode_responses=True)
        redis_code = r.get(str(mobile))
        if code != redis_code:
            raise forms.ValidationError("验证码不正确")
        return code


class RegisterGetForm(forms.Form):
    captcha = CaptchaField()


class UploadImageForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ["image"]


class RegisterPostForm(forms.Form):
    # 都被删除不用验证
    # captcha = CaptchaField()
    mobile = forms.CharField(min_length=11, max_length=11, required=True)
    code = forms.CharField(required=True, min_length=4, max_length=4)
    password = forms.CharField(required=True)

    def clean_code(self):
        # 对code进行验证
        mobile = self.data.get("mobile")
        code = self.data.get("code")
        r = redis.Redis(
            host=REDIS_HOST,
            port=REDIS_PORT,
            db=0,
            charset="utf8",
            decode_responses=True)
        redis_code = r.get(str(mobile))
        if code != redis_code:
            raise forms.ValidationError("验证码不正确")
        return code

    def clean_mobile(self):
        mobile = self.data.get("mobile")
        # filter
        user = UserProfile.objects.filter(mobile=mobile)
        if user:
            # 存在用户
            raise forms.ValidationError("用户已存在")
        return mobile


class UpdateMobileForm(forms.Form):
    # 都被删除不用验证
    # captcha = CaptchaField()
    mobile = forms.CharField(min_length=11, max_length=11, required=True)
    code = forms.CharField(required=True, min_length=4, max_length=4)

    def clean_code(self):
        # 对code进行验证
        mobile = self.data.get("mobile")
        code = self.data.get("code")
        r = redis.Redis(
            host=REDIS_HOST,
            port=REDIS_PORT,
            db=0,
            charset="utf8",
            decode_responses=True)
        redis_code = r.get(str(mobile))
        if code != redis_code:
            raise forms.ValidationError("验证码不正确")
        return code

    def clean_mobile(self):
        mobile = self.data.get("mobile")
        # filter
        user = UserProfile.objects.filter(mobile=mobile)
        if user:
            # 存在用户
            raise forms.ValidationError("用户已存在")
        return mobile


class UserInfoForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['nick_name', 'birthday', 'gender', 'address']


class UpdatePwdForm(forms.Form):
    password1 = forms.CharField(min_length=5, required=True)
    password2 = forms.CharField(min_length=5, required=True)

    def clean(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 != password2:
            raise forms.ValidationError('密碼不一致')
        return self.cleaned_data
