#!/bin/python3
# -*- coding: utf-8 -*-
from django.urls import re_path

from apps.users.views import UserInfo, UploadImageView, UpdatePwdView, UpdateMobileView, MyCourseView, MyFavOrgView, \
    MyFavTeacherView, MyFavCourseView, MyMessageView
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
urlpatterns = [
    # 个人信息
    re_path('^info/$', UserInfo.as_view(), name='info'),
    re_path('^image/upload/$', UploadImageView.as_view(), name='image'),
    re_path('^image/upload/$', UploadImageView.as_view(), name='image'),
    re_path(r'^update/pwd/$', UpdatePwdView.as_view(), name="update_pwd"),
    re_path(
        r'^update/mobile/$',
        UpdateMobileView.as_view(),
        name="update_mobile"),
    re_path(r'^mycourse$', login_required(TemplateView.as_view(
        template_name='usercenter-mycourse.html'), login_url='/login/'), {'current_page': '我的课程'}, name="mycourse"),

    re_path(r'^myfavorg/$', MyFavOrgView.as_view(), name="myfavorg"),
    re_path(r'^myfav_teacher/$', MyFavTeacherView.as_view(), name="myfav_teacher"),
    re_path(r'^myfav_course/$', MyFavCourseView.as_view(), name="myfav_course"),
    re_path(r'^my_messages/$', MyMessageView.as_view(), name="my_messages"),
]
