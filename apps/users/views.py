from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.core.paginator import PageNotAnInteger
from django.db.models import Q
from pure_pagination import Paginator

from apps.courses.models import Course
from apps.operations.models import UserCourse, UserMessage, Banner
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
import redis
# Create your views here.
from MxOline.settings import EMAIL_FROM, REDIS_HOST, REDIS_PORT, REDIS_DB
from apps.organizations.models import CourseOrg, Teacher
from apps.users.forms import LoginForm, DynamicLoginForm, DynamicLoginPostForm, RegisterGetForm, RegisterPostForm, \
    UploadImageForm, UserInfoForm, UpdatePwdForm, UpdateMobileForm
from apps.users.models import UserProfile
from apps.utils.random_str import generate_random
from apps.utils.sendemail import E_mail
from django.http import HttpResponseRedirect

class AuthBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            user = UserProfile.objects.get(Q(username=username)|Q(mobile=username))
            if user.check_password(password):
                return user
        except Exception as e:
            pass
        return None
class LogoutView(View):
    def get(self, request, *args, **kwargs):
        # 退出
        logout(request)
        # 重定向
        return HttpResponseRedirect(reverse('index'))


class LoginView(View):
    def get(self, request, *args, **kwargs):
        # 判断是否登录
        banners = Banner.objects.all()
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('index'))
        # next = / course / 5 / lesson /
        next = request.GET.get('next', '')

        # 传入验证码, 不传值不验证
        login_form = DynamicLoginForm()
        return render(
            request, 'login.html', {
                'login_form': login_form, 'next': next, 'banners': banners})

    def post(self, request, *args, **kwargs):
        # user_name = request.POST.get('username', '')
        # password = request.POST.get('password', '')
        # 验证格式
        banners = Banner.objects.all()

        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            # 返回UserProfile對象
            # 用於用戶用過用戶密碼是否密碼存在
            user_name = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            # 验证是否存在
            user = authenticate(username=user_name, password=password)
            if user is not None:
                # session
                login(request, user)
                next = request.GET.get('next')
                if next:
                    return HttpResponseRedirect(next)
                return HttpResponseRedirect(reverse('index'))
            else:
                # 未查询到用户
                d_form = DynamicLoginForm()
                return render(
                    request, 'login.html', {
                        'msg': '用户名或密码错误', 'login_form': login_form, 'banners': banners, 'd_form': d_form})
        else:
            # 做验证
            d_form = DynamicLoginForm()
            return render(request, 'login.html', {'login_form': login_form, 'banners': banners, 'd_form': d_form})


class SendSms(View):
    def post(self, request, *args, **kwargs):
        send_sms_form = DynamicLoginForm(request.POST)
        res_dict = {}
        # 验证字段
        if send_sms_form.is_valid():
            code = generate_random(4, 0)
            # 接受邮箱返回值
            res_dict = E_mail.send_text(code, '1064997626@qq.com')
            # 邮箱发送成功
            if res_dict['code']:
                mobile = send_sms_form.cleaned_data['mobile']
                # 發送郵箱
                # 鏈接redis
                r = redis.Redis(
                    host=REDIS_HOST,
                    port=REDIS_PORT,
                    db=REDIS_DB,
                    decode_responses=True)
                # 設置
                r.set(str(mobile), code)
                # 有效時間
                r.expire(str(mobile), 5 * 60)
        else:
            for k, v in send_sms_form.errors.items():
                res_dict[k] = v[0]
        return JsonResponse(res_dict)


class DynamicLoginPostView(View):
    def get(self, request, *args, **kwargs):
        banners = Banner.objects.all()
        dynamic_login = True
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse("index"))
        next = request.GET.get("next", "")
        login_form = DynamicLoginForm()

        return render(request, "login.html", {
            "login_form": login_form,
            "next": next,
            'dynamic_login': dynamic_login,
            'banners': banners
        })

    def post(self, request, *args, **kwargs):
        # true 跳转到动态登录页面
        banners = Banner.objects.all()
        dynamic_login = True
        # 判断参数是否正确
        login_form = DynamicLoginPostForm(request.POST)
        if login_form.is_valid():
            # 查询数据库是否存在
            mobile = login_form.cleaned_data['mobile']
            existed_users = UserProfile.objects.filter(mobile=mobile)
            if existed_users:
                user = existed_users[0]
            else:
                # 新建用户
                user = UserProfile(username=mobile, mobile=mobile)
                # 密码
                password = generate_random(12, 2)

                # 加密
                user.set_password(password)
                # 保存
                user.save()
            # 保存cookie
            login(request, user)
            # 重定向
            next = request.GET.get('next')
            if next:
                return HttpResponseRedirect(next)
            return HttpResponseRedirect(reverse('index'))

        else:
            # 没传值没做验证
            d_form = DynamicLoginForm()
            return render(
                request, 'login.html', {
                    'login_form': login_form, 'dynamic_login': True, 'd_form': d_form, 'banners': banners})


class RegisterView(View):
    def get(self, request, *args, **kwargs):
        register_get_form = RegisterGetForm()
        return render(
            request, 'register.html', {
                'register_get_form': register_get_form})

    def post(self, request, *args, **kwargs):
        # true 跳转到动态登录页面
        # 判断参数是否正确
        register_post_form = RegisterPostForm(request.POST)
        if register_post_form.is_valid():
            # 字段正确， 数据不存在该用户
            mobile = register_post_form.cleaned_data['mobile']
            password = register_post_form.cleaned_data['password']
            # 注册
            user = UserProfile(username=mobile)
            user.set_password(password)
            user.mobile = mobile
            # 保存
            user.save()
            # 保存cookie
            login(request, user)
            # 重定向
            return HttpResponseRedirect(reverse('index'))
        else:
            # 重定向登录
            register_get_form = RegisterGetForm()
            return render(request,
                          'register.html',
                          {'register_get_form': register_get_form,
                           'register_post_form': register_post_form})


class UserInfo(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, *args, **kwargs):
        current_page = '个人资料'
        captcha_form = RegisterGetForm
        return render(request,
                      'usercenter-info.html',
                      {'captcha_form': captcha_form,
                       'current_page': current_page})

    def post(self, request, *args, **kwargs):
        user_info_form = UserInfoForm(request.POST, instance=request.user)
        if user_info_form.is_valid():
            user_info_form.save()
            return JsonResponse({
                "status": "success"
            })
        else:
            return JsonResponse(user_info_form.errors)


class UploadImageView(LoginRequiredMixin, View):
    login_url = '/login/'

    def post(self, request, *args, **kwargs):
        image_form = UploadImageForm(
            request.POST, request.FILES, instance=request.user)
        if image_form.is_valid():
            image_form.save()
            return JsonResponse({
                "status": "success"
            })
        else:
            return JsonResponse({
                "status": "fail"
            })


class UpdatePwdView(LoginRequiredMixin, View):
    login_url = '/login/'

    def post(self, request, *args, **kwargs):
        update_pwd = UpdatePwdForm(request.POST)
        if update_pwd.is_valid():
            password = update_pwd.cleaned_data['password1']
            user = request.user
            user.set_password(password)
            user.save()
            login(request, user)
            return JsonResponse({
                "status": "success"
            })
        else:
            return JsonResponse(update_pwd.errors)


class UpdateMobileView(LoginRequiredMixin, View):
    login_url = '/login/'

    def post(self, request, *args, **kwargs):
        update_mobile_form = UpdateMobileForm(request.POST)
        if update_mobile_form.is_valid():
            mobile = update_mobile_form.cleaned_data['mobile']
            user = request.user
            if user.mobile == user.username:
                user.username = mobile
            user.mobile = mobile
            user.save()
            return JsonResponse({
                "status": "success"
            })
        else:
            return JsonResponse(update_mobile_form.errors)


class MyCourseView(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, *args, **kwargs):
        current_page = '我的课程'
        courses = UserCourse.objects.filter(user=request.user)
        return render(request, 'usercenter-mycourse.html',
                      {'courses': courses, 'current_page': current_page})


class MyFavOrgView(LoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request, *args, **kwargs):
        '''
            (1, '课程机构'),
            (2, '授课教师'),
            (3, '公开课程')
        '''
        current_page = 'my_fav_org'
        user_favorites = request.user.userfavorite_set.filter(fav_type=1)
        all_orgs = [
            CourseOrg.objects.get(
                id=fav.fav_id) for fav in user_favorites]
        courses = UserCourse.objects.filter(user=request.user)
        return render(request, 'usercenter-fav-org.html',
                      {'courses': courses, 'current_page': current_page, 'all_orgs': all_orgs})


class MyFavTeacherView(LoginRequiredMixin, View):
    login_url = '/login/'
    def get(self, request, *args, **kwargs):
        '''
            (1, '课程机构'),
            (2, '授课教师'),
            (3, '公开课程')
        '''
        current_page = 'my_fav_teacher'
        user_favorites = request.user.userfavorite_set.filter(fav_type=2)
        all_teachers = [
            Teacher.objects.get(
                id=fav.fav_id) for fav in user_favorites]
        courses = UserCourse.objects.filter(user=request.user)
        return render(request, 'usercenter-fav-teacher.html',
                      {'courses': courses, 'current_page': current_page, 'all_teachers': all_teachers})


class MyFavCourseView(LoginRequiredMixin, View):
    login_url = '/login/'
    def get(self, request, *args, **kwargs):
        '''
            (1, '课程机构'),
            (2, '授课教师'),
            (3, '公开课程')
        '''
        current_page = 'my_fav_course'
        user_favorites = request.user.userfavorite_set.filter(fav_type=3)

        all_courses = [Course.objects.get(id=fav.fav_id) for fav in user_favorites]
        courses = UserCourse.objects.filter(user=request.user)
        return render(request, 'usercenter-fav-course.html',
                      {'courses': courses, 'current_page': current_page, 'all_courses': all_courses})


def message_nums(request):
    """
    Add media-related context variables to the context.
    """
    if request.user.is_authenticated:
        return {'unread': request.user.usermessage_set.filter(has_read=False).count()}
    return {}

class MyMessageView(LoginRequiredMixin, View):
    login_url = '/login/'
    def get(self, request, *args, **kwargs):
        # 所有信息
        messages = UserMessage.objects.filter(user=request.user).order_by('-add_time')
        current_page = "message"
        for message in messages:
            message.has_read = True
            message.save()
        # 对讲师数据进行分页
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1

        p = Paginator(messages, per_page=10, request=request)
        messages = p.page(page)

        return render(request, "usercenter-message.html", {
            "messages": messages,
            "current_page": current_page
        })
