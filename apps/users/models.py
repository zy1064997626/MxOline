from datetime import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
GENDER_CHOICES = (
    ('male', '男'),
    ('female', '女')
)


class BaseModel(models.Model):
    add_time = models.DateTimeField(default=datetime.now, verbose_name='添加时间')

    class Meta:
        abstract = True
# us


class UserProfile(AbstractUser):
    '''
    昵       称：
    生       日：
    性       别
    地       址：
    手  机  号
    头像
    '''
    # CharField 一定要设置max_length
    # 默认null=False, black=False
    nick_name = models.CharField(max_length=20, default='', verbose_name='昵称', blank=True)
    birthday = models.DateField(null=True, blank=True, verbose_name='生日')
    gender = models.CharField(
        choices=GENDER_CHOICES,
        max_length=6,
        verbose_name='性别')
    address = models.CharField(max_length=100, verbose_name='地址', default='')
    # 手机注册必填值, 唯一
    mobile = models.CharField(max_length=11, verbose_name='手机号码')
    image = models.ImageField(
        verbose_name='头像',
        upload_to='head_image/%Y/%m',
        default='defalut.jpg')

    class Meta:
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def unread(self):
        return self.usermessage_set.filter(has_read=False).count()
    def __str__(self):
        if self.nick_name:
            return self.nick_name
        else:
            return self.username
