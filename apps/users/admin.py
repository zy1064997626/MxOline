from django.contrib import admin
from apps.users.models import UserProfile
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model
UserProfile = get_user_model()
# Register your models here.


class UserProfileAdmin(admin.ModelAdmin):
    pass
#
#
# # https://www.cnblogs.com/feifeifeisir/p/12870181.html
# admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(UserProfile, UserAdmin)
