#!/bin/python3
# -*- coding: utf-8 -*-

from django import forms
from apps.operations.models import UserAsk
import re


class AddAskForm(forms.ModelForm):
    mobile = forms.CharField(max_length=11,
                             min_length=11, required=True, )

    class Meta:
        model = UserAsk
        # fields = ['name', 'mobile', 'course']
        exclude = ['add_time']

    def clean_mobile(self):
        mobile_regex = r'^1(3|4|5|7|8)\d{9}$'
        p = re.compile(mobile_regex)
        mobile = self.data.get('mobile')
        if p.match(mobile):
            return mobile
        else:
            raise forms.ValidationError('手机号码不匹配', code='mobile_valid')
