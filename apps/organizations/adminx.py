#!/bin/python3
# -*- coding: utf-8 -*-
from .models import Teacher, CourseOrg, City
import xadmin


class TeacherAdmin(object):
    list_display = [
        'name',
        'org',
        'work_years',
        'work_company']
    search_fields = [
        'name',
        'org',
        'work_years',
        'work_company']
    list_filter = [
        'name',
        'org__name',
        'work_years',
        'work_company']


class CourseOrgAdmin(object):
    list_display = ['name', 'desc', 'click_nums', 'fav_nums']
    search_fields = ['name', 'desc', 'click_nums', 'fav_nums']
    list_filter = ['name', 'desc', 'click_nums', 'fav_nums']


class CityAdmin(object):
    list_display = ['id', 'name', 'desc']
    search_fields = ['name', 'desc']
    list_filter = ['name', 'desc', 'add_time']
    list_editable = ['name', 'desc']


xadmin.site.register(CourseOrg, CourseOrgAdmin)
xadmin.site.register(City, CityAdmin)
xadmin.site.register(Teacher, TeacherAdmin)
