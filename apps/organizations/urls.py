#!/bin/python3
# -*- coding: utf-8 -*-
from django.urls import re_path

from apps.organizations.views import OrgView, AddAskView, OrgHomeView, OrgTeacherView, OrgCourseView, OrgDescView, \
    TeacherView, TeacherDetailView

urlpatterns = [
    re_path('^list/$', OrgView.as_view(), name='list'),

    re_path('^add_ask/$', AddAskView.as_view(), name='add_ask'),
    re_path('^(?P<org_id>\d+)/$', OrgHomeView.as_view(), name='home'),
    re_path('^(?P<org_id>\d+)/teacher/$', OrgTeacherView.as_view(), name='teacher'),
    re_path('^(?P<org_id>\d+)/course/$', OrgCourseView.as_view(), name='course'),
    re_path('^(?P<org_id>\d+)/desc/$', OrgDescView.as_view(), name='desc'),


    # 老师
    re_path('^teachers/$', TeacherView.as_view(), name='teachers'),
    re_path('^teachers/(?P<teacher_id>\d+)/$', TeacherDetailView.as_view(), name='teacher_detail'),
]
