from django.db import models



from apps.users.models import BaseModel, UserProfile

CATEGORY_CHOICES = (
    ('pxjg', '培训机构'),
    ('gx', '高校'),
    ('gr', '个人')
)


class City(BaseModel):
    name = models.CharField(max_length=20, verbose_name='城市名')
    desc = models.CharField(max_length=200, verbose_name="描述")

    class Meta:
        verbose_name = '城市'
        verbose_name_plural = verbose_name

    def __str__(self):
        ''':return 值为必填项'''
        return self.name


class CourseOrg(BaseModel):
    '''
    机构名称
    描述
    机构标签
    机构类别  培训机构 高校 个人
    点击数
    收藏数
    机构头像
    机构地址
    学习人数
    课程数
    所在地区
    是否认证
    是否金牌
    '''
    name = models.CharField(verbose_name='机构名称', max_length=20)
    desc = models.TextField(verbose_name='机构描述')
    tag = models.CharField(default='全国知名', max_length=10, verbose_name='机构标签')
    is_auth = models.BooleanField(default=False, verbose_name='是否认证')
    is_gold = models.BooleanField(default=False, verbose_name='是否金牌')
    category = models.CharField(
        choices=CATEGORY_CHOICES,
        max_length=4,
        default=CATEGORY_CHOICES[0][0])
    click_nums = models.IntegerField(default=0, verbose_name='点击数量')
    fav_nums = models.IntegerField(default=0, verbose_name='收藏数')
    image = models.ImageField(
        verbose_name='机构头像',
        upload_to='org/%Y/%m',
        max_length=100)
    address = models.CharField(verbose_name='机构地址', max_length=100)
    students = models.IntegerField(verbose_name='学习人数', default=0)
    course_nums = models.IntegerField(verbose_name='课程数量', default=0)
    city = models.ForeignKey(
        City,
        verbose_name='所在地区',
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = '课程机构'
        verbose_name_plural = verbose_name
    def courses(self):
        # from apps.courses.models import Course
        # courses = Course.objects.all().filter(course_org=self)
        # courses = self.course_set.filter(course_org=self)
        # courses = self.course_set.filter(course_org_id=self.id)
        '''经典课程'''
        courses = self.course_set.filter(is_classics=True)
        return courses



    def __str__(self):
        return self.name


class Teacher(BaseModel):
    '''
    所属机构
    教师姓名
    工作年限
    就职公司
    公司职位
    教学特点
    点击数
    收藏数
    年龄
    头像
    '''
    org = models.ForeignKey(
        CourseOrg,
        verbose_name='所属机构',
        on_delete=models.CASCADE)
    name = models.CharField(verbose_name='教师名称', max_length=20)
    user = models.OneToOneField(UserProfile, null=True, blank=True,  on_delete=models.SET_NULL, verbose_name='用户')
    work_years = models.IntegerField(verbose_name='工作年限', default=0)
    work_company = models.CharField(max_length=50, verbose_name='就职公司')
    work_position = models.CharField(max_length=50, verbose_name="公司职位", default='python开发')
    points = models.CharField(max_length=50, verbose_name='教学特点')
    click_nums = models.IntegerField(default=0, verbose_name='点击数')
    fav_nums = models.IntegerField(default=0, verbose_name='收藏数')
    age = models.IntegerField(default=18, verbose_name='年纪')
    image = models.ImageField(
        default='',
        verbose_name='教师头像',
        upload_to='teacher',
        max_length=100)

    class Meta:
        verbose_name = '教师'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
    def course_nums(self):
        return self.course_set.count()