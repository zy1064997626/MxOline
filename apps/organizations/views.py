from django.db.models import Q
from django.shortcuts import render
from django.views.generic.base import View
# Create your views here.
from apps.organizations.forms import AddAskForm
from apps.organizations.models import CourseOrg, City, Teacher
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from apps.operations.models import UserFavorite


class TeacherDetailView(View):
    def get(self, request, teacher_id, *args, **kwargs):
        teacher = Teacher.objects.get(id=teacher_id)
        teacher.click_nums += 1
        teacher.save()
        teacher_fav = False
        org_fav = False
        '''
            (1, '课程机构'),
            (2, '授课教师'),
            (3, '公开课程')
        '''
        if request.user.is_authenticated:
            if UserFavorite.objects.filter(
                    user=request.user, fav_type=2, fav_id=teacher_id):
                teacher_fav = True
            if UserFavorite.objects.filter(
                    user=request.user,
                    fav_type=1,
                    fav_id=teacher.org_id):
                org_fav = True
        hot_teachers = Teacher.objects.all().order_by('-click_nums')
        return render(request,
                      'teacher-detail.html',
                      {'teacher': teacher,
                       'teacher_fav': teacher_fav,
                       'org_fav': org_fav,
                       'hot_teachers': hot_teachers})


class TeacherView(View):
    def get(self, request, *args, **kwargs):
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        all_teachers = Teacher.objects.all()
        keywords = request.GET.get('keywords')
        s_type = ''
        if keywords:
            all_teachers = all_teachers.filter(Q(name__icontains=keywords))
            s_type = 'teacher'
        hot_teachers = Teacher.objects.all().order_by('-click_nums')
        # 排序
        sort = request.GET.get('sort', '')
        # 学习人数排序
        if sort == 'hot':
            # 降序
            all_teachers = all_teachers.order_by('-click_nums')
        teachers_nums = Teacher.objects.count()
        p = Paginator(all_teachers, per_page=1, request=request)
        all_teachers = p.page(page)
        return render(request,
                      'teachers-list.html',
                      {'all_teachers': all_teachers,
                       'teachers_nums': teachers_nums,
                       'sort': sort, 'hot_teachers': hot_teachers, 'keywords': keywords, 's_type': s_type})


class OrgView(View):
    def get(self, request, *args, **kwargs):
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        all_orgs = CourseOrg.objects.all()
        keywords = request.GET.get("keywords", "")
        s_type = ''
        if keywords:
            all_orgs = all_orgs.filter(Q(name__icontains=keywords) | Q(desc__icontains=keywords))
            s_type = 'org'
        # 根据点击数量
        hot_orgs = all_orgs.order_by('-click_nums')
        # category筛选
        category = request.GET.get('ct', '')
        if category:
            all_orgs = all_orgs.filter(category=category)
        city_id = request.GET.get('city', '')
        if city_id and city_id.isdigit():
            all_orgs = all_orgs.filter(city_id=int(city_id))
            # 选中状态
        # 排序
        sort = request.GET.get('sort', '')
        # 学习人数排序
        if sort == 'students':
            # 降序
            all_orgs = all_orgs.order_by('-students')
        elif sort == 'courses':
            # 降序
            all_orgs = all_orgs.order_by('-course_nums')
        p = Paginator(all_orgs, per_page=10, request=request)
        all_orgs = p.page(page)
        org_nums = CourseOrg.objects.count()
        all_citys = City.objects.all()
        return render(request,
                      'org-list.html',
                      {'all_orgs': all_orgs,
                       'org_nums': org_nums,
                       'all_citys': all_citys,
                       'category': category,
                       'city_id': city_id,
                       'sort': sort, 'hot_orgs': hot_orgs,
                       'keywords': keywords, 's_type': s_type})


class AddAskView(View):
    '''
    用户咨询
    '''

    def post(self, request, *args, **kwargs):
        user_ask_form = AddAskForm(request.POST)
        if user_ask_form.is_valid():
            # 保存到数据库
            user_ask = user_ask_form.save(commit=True)
            # 返回信息
            return JsonResponse({
                'status': 'success',
            })
        else:
            return JsonResponse({'status': 'fail', 'msg': ';'.join(
                [v[0] for k, v in user_ask_form.errors.items()])})


class OrgHomeView(View):
    def get(self, request, org_id, *args, **kwargs):

        current_page = 'home'
        # org_id是字符串类型， 要转换
        org = CourseOrg.objects.get(id=int(org_id))
        has_fav = False
        if request.user.is_authenticated:
            if UserFavorite.objects.filter(
                    user=request.user, fav_id=org.id, fav_type=1):
                has_fav = True
        # 点击次数+1
        org.click_nums += 1
        org.save()
        # 和机构相关所有的课程
        all_courses = org.course_set.all()
        # 和机构相关的所有老师
        all_teachers = org.teacher_set.all()

        return render(request,
                      'org-detail-homepage.html',
                      {'org': org,
                       'all_courses': all_courses,
                       'all_teachers': all_teachers,
                       'current_page': current_page, 'has_fav': has_fav})


class OrgTeacherView(View):
    def get(self, request, org_id, *args, **kwargs):
        current_page = 'teacher'
        org = CourseOrg.objects.get(id=int(org_id))
        # 点击次数+1
        has_fav = False
        if request.user.is_authenticated:
            if UserFavorite.objects.filter(
                    user=request.user, fav_id=org.id, fav_type=1):
                has_fav = True
        org.click_nums += 1
        org.save()
        all_teachers = org.teacher_set.all()
        return render(request,
                      'org-detail-teachers.html',
                      {'all_teachers': all_teachers,
                       'org': org,
                       'current_page': current_page, 'has_fav': has_fav})


class OrgCourseView(View):
    def get(self, request, org_id, *args, **kwargs):
        current_page = 'course'
        # org_id是字符串类型， 要转换
        org = CourseOrg.objects.get(id=int(org_id))
        has_fav = False
        if request.user.is_authenticated:
            if UserFavorite.objects.filter(
                    user=request.user, fav_id=org.id, fav_type=1):
                has_fav = True
        # 点击次数+1
        org.click_nums += 1
        org.save()
        # 和机构相关所有的课程
        all_courses = org.course_set.all()
        # 分页
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        p = Paginator(all_courses, per_page=1, request=request)
        all_courses = p.page(page)
        return render(request,
                      'org-detail-course.html',
                      {'all_courses': all_courses,
                       'current_page': current_page,
                       'org': org, 'has_fav': has_fav})


class OrgDescView(View):
    def get(self, request, org_id, *args, **kwargs):
        current_page = 'desc'
        # org_id是字符串类型， 要转换
        org = CourseOrg.objects.get(id=int(org_id))
        # 点击次数+1
        org.click_nums += 1
        org.save()
        # 和机构相关所有的课程
        has_fav = False
        if request.user.is_authenticated:
            if UserFavorite.objects.filter(
                    user=request.user, fav_id=org.id, fav_type=1):
                has_fav = True
        return render(request,
                      'org-detail-desc.html',
                      {
                          'current_page': current_page,
                          'org': org, 'has_fav': has_fav})
