#!/bin/python3
# -*- coding: utf-8 -*-
from django import forms

from apps.operations.models import UserFavorite, UserComment


class UserFavForm(forms.ModelForm):
    class Meta:
        model = UserFavorite
        fields = ['fav_id', 'fav_type']


class CommentForm(forms.ModelForm):
    class Meta:
        model = UserComment
        fields = ['course', 'comment']