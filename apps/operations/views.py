from django.shortcuts import render

# Create your views here.
from django import views
from django.http import JsonResponse

from apps.operations.forms import UserFavForm, CommentForm
from apps.operations.models import UserFavorite, UserComment, Banner
from apps.organizations.models import CourseOrg, Teacher
from apps.courses.models import Course
class CommentView(views.View):
    def post(self, request, *args, **kwargs):
        # 首先判断用户是否登录
        if request.user.is_authenticated:
            # 表单验证
            comment_form = CommentForm(request.POST)
            if comment_form.is_valid():
                # 验证成功
                # 判断是否已经收藏过了
                course = comment_form.cleaned_data['course']
                comment = comment_form.cleaned_data['comment']
                user_comment = UserComment()
                user_comment.user = request.user
                user_comment.course = course
                user_comment.comment = comment
                user_comment.save()
                return JsonResponse({
                    "status": "success",
                })
            else:
                return JsonResponse({
                    "status": "fail",
                    "msg": "参数错误"
                })
        else:
            return JsonResponse({'status': 'fail', 'msg': '用户未登录'})

class AddFavView(views.View):
    def post(self, request, *args, **kwargs):
        # 首先判断用户是否登录
        if request.user.is_authenticated:
            # 表单验证
            add_fav_form = UserFavForm(request.POST)
            if add_fav_form.is_valid():
                # 验证成功
                # 判断是否已经收藏过了
                fav_id = add_fav_form.cleaned_data['fav_id']
                fav_type = add_fav_form.cleaned_data['fav_type']
                exited_favs = UserFavorite.objects.filter(
                    fav_id=fav_id, fav_type=fav_type, user=request.user)
                if exited_favs:
                    # 取消
                    # 删除
                    '''
                        (1, '课程机构'),
                        (2, '授课教师'),
                        (3, '公开课程')
                    '''
                    exited_favs.delete()
                    if fav_type == 1:
                        course_org = CourseOrg.objects.get(id=fav_id)
                        course_org.fav_nums -= 1
                        course_org.save()
                    elif fav_type == 2:
                        teacher = Teacher.objects.get(id=fav_id)
                        teacher.fav_nums -= 1
                        teacher.save()
                    elif fav_type == 3:
                        course = Course.objects.get(id=fav_id)
                        course.fav_nums -= 1
                        course.save()
                    return JsonResponse({
                        "status": "success",
                        "msg": "收藏"
                    })
                else:
                    # 收藏
                    user_fav = UserFavorite()
                    user_fav.fav_type = fav_type
                    user_fav.fav_id = fav_id
                    user_fav.user = request.user
                    user_fav.save()
                    if fav_type == 1:
                        course_org = CourseOrg.objects.get(id=fav_id)
                        course_org.fav_nums += 1
                        course_org.save()
                    elif fav_type == 2:
                        teacher = Teacher.objects.get(id=fav_id)
                        teacher.fav_nums += 1
                        teacher.save()
                    elif fav_type == 3:
                        course = Course.objects.get(id=fav_id)
                        course.fav_nums += 1
                        course.save()
                    return JsonResponse({
                        "status": "success",
                        "msg": "已收藏",
                    })

            else:
                return JsonResponse({
                    "status": "fail",
                    "msg": "参数错误"
                })
        else:
            return JsonResponse({'status': 'fail', 'msg': '用户未登录'})


class IndexView(views.View):
    def get(self, request, *args, **kwargs):
        banners = Banner.objects.all().order_by("index")
        courses = Course.objects.filter(is_banner=False)[:6]
        banner_courses = Course.objects.filter(is_banner=True)
        course_orgs = CourseOrg.objects.all()[:15]
        return render(request, 'index.html', {
            'banners': banners,
            'courses': courses,
            'banner_courses': banner_courses,
            'course_orgs': course_orgs
        })