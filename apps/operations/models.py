from django.db import models
from django.contrib.auth import get_user_model

from apps.users.models import BaseModel
from apps.courses.models import Course
# Create your models here.
# 使用外键
# https://www.cnblogs.com/robinunix/p/7922403.html
# https://docs.djangoproject.com/zh-hans/2.2/topics/auth/customizing/
UserProfile = get_user_model()


class UserAsk(BaseModel):
    name = models.CharField(verbose_name='名称', max_length=20)
    mobile = models.CharField(max_length=11, verbose_name='电话号码')
    course = models.CharField(max_length=20, verbose_name='课程名称')

    class Meta:
        verbose_name = '用户咨询'
        verbose_name_plural = verbose_name

    def __str__(self):
        return f'{self.name}_{self.mobile}_{self.course}'


class UserComment(BaseModel):
    course = models.ForeignKey(
        Course,
        verbose_name='课程名称',
        on_delete=models.CASCADE)
    user = models.ForeignKey(
        UserProfile,
        verbose_name='用户',
        on_delete=models.CASCADE)
    comment = models.CharField(max_length=200, verbose_name='评论')

    class Meta:
        verbose_name = '用户评论'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.comment




USER_FAVORITE_CHOICES = (
    (1, '课程机构'),
    (2, '授课教师'),
    (3, '公开课程')
)


class UserFavorite(BaseModel):
    user = models.ForeignKey(
        UserProfile,
        verbose_name='用户',
        on_delete=models.CASCADE)
    fav_id = models.IntegerField(verbose_name='数据id')
    fav_type = models.IntegerField(
        choices=USER_FAVORITE_CHOICES,
        verbose_name='收藏类型',
        default=1)

    class Meta:
        verbose_name = '用户收藏'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "{user}_{id}".format(user=self.user.username, id=self.fav_id)


class UserCourse(BaseModel):
    user = models.ForeignKey(
        UserProfile,
        verbose_name='用户',
        on_delete=models.CASCADE)
    course = models.ForeignKey(
        Course,
        verbose_name='课程',
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = '用户课程'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.course.name


class UserMessage(BaseModel):
    user = models.ForeignKey(
        UserProfile,
        on_delete=models.CASCADE,
        verbose_name="用户")
    message = models.CharField(max_length=200, verbose_name="消息内容")
    has_read = models.BooleanField(default=False, verbose_name="是否已读")

    class Meta:
        verbose_name = "用户消息"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.message



class Banner(BaseModel):

    title = models.CharField(max_length=100, verbose_name="标题")
    image = models.ImageField(upload_to="banner/%Y/%m", max_length=200, verbose_name="轮播图")
    url = models.URLField(max_length=200, verbose_name="访问地址")
    index = models.IntegerField(default=0, verbose_name="顺序")

    class Meta:
        verbose_name = "轮播图"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title