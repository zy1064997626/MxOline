#!/bin/python3
# -*- coding: utf-8 -*-
from django.urls import re_path, path

from apps.operations.views import AddFavView, CommentView

urlpatterns = [
    re_path('^fav/$', AddFavView.as_view(), name='fav'),
    re_path('^comment/$', CommentView.as_view(), name='comment'),

]
