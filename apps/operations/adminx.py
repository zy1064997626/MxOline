#!/bin/python3
# -*- coding: utf-8 -*-
from .models import UserAsk, UserComment, UserFavorite, UserCourse, UserMessage, Banner
import xadmin
# Register your models here.


class UserAskAdmin(object):
    list_display = ['name', 'mobile', 'course', 'add_time']
    search_fields = ['name', 'mobile', 'course']
    list_filter = ['name', 'mobile', 'course', 'add_time']


class UserCommentAdmin(object):
    list_display = ['user', 'course', 'comments', 'add_time']
    search_fields = ['user', 'course', 'comment']
    list_filter = ['user', 'course', 'comment', 'add_time']


class UserFavoriteAdmin(object):
    list_display = ['user', 'fav_id', 'fav_type', 'add_time']
    search_fields = ['user', 'fav_id', 'fav_type']
    list_filter = ['user', 'fav_id', 'fav_type', 'add_time']


class UserCourseAdmin(object):
    list_display = ['user', 'course', 'add_time']
    search_fields = ['user', 'course']
    list_filter = ['user', 'course', 'add_time']
    # 编辑保存
    def save_models(self):
        obj = self.new_obj
        if not obj.id:
            obj.save()
            course = obj.course
            course.students += 1
            course.save()


class UserMessageAdmin(object):
    list_display = ['user', 'message', 'has_read', 'add_time']
    search_fields = ['user', 'message', 'has_read']
    list_filter = ['user', 'message', 'has_read', 'add_time']

class BannerAdmin(object):
    list_display = ['title', 'image', 'url', "index"]
    search_fields = ['title', 'image', 'url', "index"]
    list_filter = ['title', 'image', 'url', "index"]
class GlobalSetting(object):
    site_title = '大禹网'
    site_footer = 'xy网'
    # menu_style = "accordion"



class BaseSetting(object):
    enable_themes = True  # 启动主题
    use_bootswatch = True  # 启动主题样板


# UserAsk, UserComment, UserFavorite, UserCourse,  UserMessage
xadmin.site.register(UserAsk, UserAskAdmin)
xadmin.site.register(UserComment, UserCommentAdmin)
xadmin.site.register(UserFavorite, UserFavoriteAdmin)
xadmin.site.register(UserCourse, UserCourseAdmin)
xadmin.site.register(UserMessage, UserMessageAdmin)
xadmin.site.register(Banner, BannerAdmin)
xadmin.site.register(xadmin.views.CommAdminView, GlobalSetting)
xadmin.site.register(xadmin.views.BaseAdminView, BaseSetting)
